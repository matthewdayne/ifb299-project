<?php
	include "header.php";
?>
<script>
     
  function changePic(imgName)
  {
     image = document.getElementById('bigIMG');
     image.src = imgName;
  }
 
</script>

<div id="middle">
	<div>
		<table>
			<tr>
				<td rowspan="5">
<?php
	$files = glob('images/properties/'. $_GET["id"] .'/*.{jpg,png,gif}', GLOB_BRACE);
	echo <<< END
			<img src="$files[0]" id="bigIMG">
END;
?>
				</td>
			</tr>
<?php
	$files = glob('images/properties/'. $_GET["id"] .'/*.{jpg,png,gif}', GLOB_BRACE);
	foreach($files as $file) {
	echo <<< END
	<tr>
		<td>
			<img id="browseimg" src="$file" onclick="changePic('$file')">
		</td>
	</tr>
END;
	}
?>
		</table>
	</div>
		<?php
		$getID = intval(htmlspecialchars($_GET["id"]));
		/* The follow sql code is used to access the data of the location specified held in the database. */
		$stm = $pdo->prepare("SELECT * FROM properties WHERE propertiesID=$getID");
		$stm->execute();
		$data = $stm->fetchAll();
		$data = $data[0];
		
		echo <<< END
		<table style="width:90%; margin-left: 5%; margin-right: 5%;">
		<tr>
			
			<td>$data[StreetAddress]</td>
			<td>$data[Suburb]</td>
			<td>$data[Type]</td>
			<td>$data[Bedrooms] <i class="fa fa-bed"></i></td>
			<td>$data[Bathrooms] Baths</td>
			<td>$data[Parking] <i class="fa fa-car"></i></td>
			<td>$data[Description]</td>
			<td>$$data[Rent]</td>
		</tr>
		</table>
END;
	?>
</div>
<?php
	include "footer.php";
?>