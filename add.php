<?php
session_start();
if(!($_SESSION['username']=='DaveCEO' && $_SESSION['password']=='JOHNCENA')) {
	header ("Location: signin.php");
}

include "header.php";
?>

<script type="text/javascript" >

        function insertProperty(form) 
        {
            $.ajax({
                url: "insert.php",
                type: "POST",
                data: {
				'address': form.address.value,
				'suburb': form.suburb.value,
				'type': form.type.value,
				'bedrooms': form.bedrooms.value,
				'bathrooms': form.bathrooms.value,
				'parking': form.parking.value,
				'rent': form.rent.value,
				'description': form.description.value
				},                   
                success: function(data)
                            {
                                alert("Property Inserted");                                    
                            }
            });
        }

</script>

<div id="middle">

<table>
	<tr><TH COLSPAN=5><hr></th></tr>
	<tr>
	<form action="javascript:insertProperty(this.form);">
	<td>Street Address <input type="text" name="address" value=""></td>
	<td>Suburb
	<select name="suburb">
	<?php
		$stm = $pdo->prepare("SELECT suburbName FROM suburbs");
			$stm->execute();
			$data = $stm->fetchAll();
			foreach($data as $row){
				echo <<< END
				<option value="$row[suburbName]">$row[suburbName]</option>
END;
			}
	?>
	</select></td>
	<td>Property Type <select name="type">
		<option value="House">House</option>
		<option value="House">Apartment</option>
		<option value="House">Unit</option>
	</select></td>
	</tr>
	<tr>
	<td>Bedrooms <input type="number" name="bedrooms" value="" min="1" max="10"></td>
	<td>Bathrooms <input type="number" name="bathrooms" value="" min="1" max="10"></td>
	<td>Parking <input type="number" name="parking" value="" min="1" max="10"></td>
	<td>Rent <input type="number" name="rent" value="" min="0" max="5000"></td>
	</tr>
	<tr><td>Description</td></tr>
	<tr><td colspan=5><textarea rows="10" cols="100" name="description" value=""></textarea></td></tr>
	<tr>
	<td><input type="submit" name="" value="save edit" onclick=insertProperty(this.form);></td>
	</form>
	</tr>
</table>

</div>
<?php
	include "footer.php";
?>	