<?php
session_start();
if(!($_SESSION['username']=='DaveCEO' && $_SESSION['password']=='JOHNCENA')) {
	header ("Location: signin.php");
}

include "header.php";
?>
<script type="text/javascript" >

        function editAccount(form) 
        {
            $.ajax({
                url: "inc/updateAccount.php",
                type: "POST",
                data: { 
				'id': form.userID.value,
				'accountType': form.accountType.value,
				'phone': form.phone.value,
				'email': form.email.value,
				'password': form.password.value
				},                   
                success: function(data)
                            {
                                alert("Account Updated");
                            }
            });
        }
</script>
<div id="middle">
	<!-- Insert content for browse page below here. -->
	<table style="width:90%; margin-left: 5%; margin-right: 5%;" cellspacing="0" id="ceotable">
	<tr>
		<td colspan="2"></td>
		<td colspan="3"><b>Create new account:</b></td>
		<td><a href="createAccount.php"><i class="fa fa-plus" style="color:green; font-size: 300%;"></i></a></td>
	</tr>
<?php
	$formID = 0;
			/* This pdo statement is used to query the database for all locations data. */
			$stm = $pdo->prepare("SELECT * FROM users");
			$stm->execute();
			$data = $stm->fetchAll();
			foreach($data as $row){
			$formID = $formID +1;
			echo <<< END
					<tr>
					<form id="form$formID" action="javascript:editAccount(document.forms['form$formID']);">
						<td>
						<input type="hidden" name="userID" value="$row[userID]"></input>
						<b>Username:</b> $row[username]</td>
						<td><b>Accout Type:</b>
						<select name="accountType">
END;
// Start of account type dropbox
if ($row['accountType'] == "Tenant"){
	echo <<< END
	<option value="Tenant" selected>Tenant</option>
END;
} else {
	echo <<< END
						<option value="Tenant">Tenant</option>
END;
}

if ($row['accountType'] == "Owner"){
	echo <<< END
						<option value="Owner" selected>Owner</option>
END;
} else {
	echo <<< END
						<option value="Owner">Owner</option>
END;
}

if ($row['accountType'] == "Staff"){
echo <<< END
						<option value="Staff" selected>Staff</option>
END;
} else {
echo <<< END
						<option value="Staff">Staff</option>
END;
}
if ($row['accountType'] == "CEO"){
echo <<< END
						<option value="CEO" selected>CEO</option>
END;
} else {
echo <<< END
						<option value="CEO">CEO</option>
END;
}
echo <<< END
						</select>
END;
//End of account type dropbox
echo <<< END
						</td>
						<td><b>Phone Number:</b> <input type="text" value="$row[phone]" name="phone"></input></td>
						<td><b>Email:</b> <input type="text" value="$row[email]" name="email"></input></td>
						<td><b>Password:</b> <input type="password" value="$row[password]" name="password"></input></td>
						<td><i class="fa fa-floppy-o" onclick="document.forms['form$formID'].submit();"></i></td>
					</form>
					</tr>
END;
			}
?>
		</table>
	</div>

<?php
	include "footer.php";
?>