<!DOCTYPE HTML>
<head>
	<title>David's Real Estate</title>
	<link rel="shortcut icon" href="images/whitehouse.png">
	<link href="css/StyleSheet.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<?php
	require "inc/pdo.inc";
	if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}
?>
</head>
<body>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>
<?php
	if (isset($_SESSION['username']) && isset($_SESSION['password'])){
		if(!($_SESSION['username']=='DaveCEO' && $_SESSION['password']=='JOHNCENA')) {
			include "headers/notlogged.php";
		} else {
			include "headers/ceologged.php";
		}
	} else {
		include "headers/notlogged.php";
	}
	
	
	include "search.php";
?>