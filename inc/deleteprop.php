<?php
require "pdo.inc";

$stmt = $pdo->prepare("DELETE FROM properties WHERE propertiesID = ?");
$stmt->bindparam(1, $_POST['id']);
$stmt->execute();
unset($stmt);

$target_dir = "../images/properties/". $_POST["id"] . "/";
if (file_exists($target_dir)) {
	$it = new RecursiveDirectoryIterator($target_dir, RecursiveDirectoryIterator::SKIP_DOTS);
	$files = new RecursiveIteratorIterator($it,
				 RecursiveIteratorIterator::CHILD_FIRST);
	foreach($files as $file) {
		if ($file->isDir()){
			rmdir($file->getRealPath());
		} else {
			unlink($file->getRealPath());
		}
	}
	rmdir($target_dir);
}
?>