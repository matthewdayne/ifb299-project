<?php
require "pdo.inc";

$username = htmlspecialchars($_POST['username']);
$accountType = htmlspecialchars($_POST['accountType']);
$password = htmlspecialchars($_POST['password']);
$phone = htmlspecialchars($_POST['phone']);
$email = htmlspecialchars($_POST['email']);

$sql = "INSERT INTO users
		(username, accountType, password, joinDate, phone, email)
		VALUES
		(?, ?, ?, ?, ?, ?)";

$upd = $pdo->prepare($sql);
$today = getdate();
$date = $today['year']  . '-' . $today['mon'] . '-' . $today['mday'];
echo $date;
$upd->execute(array($username, $accountType, $password, $date, $phone, $email));
?>