<?php
require "pdo.inc";

$id = htmlspecialchars($_POST['id']);
$accountType = $_POST['accountType'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$password = $_POST['password'];


$sql = "UPDATE 
			users
		SET 
			accountType = ?, 
			phone = ?, 
			email = ?, 
			password = ?
		WHERE 
			userID = ?";

$upd = $pdo->prepare($sql);

$upd->execute(array($accountType, $phone, $email, $password, $id));
?>