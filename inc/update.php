<?php
require "pdo.inc";

$id = htmlspecialchars($_POST['id']);
$address = htmlspecialchars($_POST['address']);
$suburb = $_POST['suburb'];
$type = $_POST['type'];
$bedrooms = $_POST['bedrooms'];
$bathrooms = $_POST['bathrooms'];
$parking = $_POST['parking'];
$rent = $_POST['rent'];
$description = $_POST['description'];

$sql = "UPDATE 
			properties
		SET 
			StreetAddress = ?, 
			Suburb = ?, 
			Type = ?, 
			Bedrooms = ?, 
			Bathrooms = ?, 
			Parking = ?, 
			Rent = ?, 
			Description = ? 
		WHERE 
			propertiesID = ?";

$upd = $pdo->prepare($sql);

$upd->execute(array($address, $suburb, $type, $bedrooms, $bathrooms, $parking, $rent, $description, $id));
?>