<?php
	include "header.php";
?>
<!-- Insert content for main page below here. -->
<div id="middle">
	<table align="center"> 
		<tr>
			<td> <img id="DaveDP" src="images/Suitphoto.png"> </td> 
			<td>
				<p class="bubble"> David�s property management believes great people make a great business. Our agents are talented, dedicated and community minded professionals. We value integrity, 
				trust and sincerity as much as we value hard work, determination and success. We rent simple studios and multi-million dollar homes with the same commitment, professionalism and attention 
				to detail. We believe outstanding presentation, strategic marketing, genuine client service and superior sales skills are the core ingredients of a premium outcome at any price level.</p> 
		</tr> 
	</table> 
</div>
<!-- Insert content for main page above here. -->
<?php
	include "footer.php";
?>