<?php
session_start();
if(!($_SESSION['username']=='DaveCEO' && $_SESSION['password']=='JOHNCENA')) {
	header ("Location: signin.php");
}

include "header.php";
?>
<script type="text/javascript">
	function deleteproperty(pid) 
        {
			r = confirm("Are you sure you want to delete property " + pid + "?");
			if (r == true){
				$.ajax({
					url: "inc/deleteprop.php",
					type: "POST",
					data: { 
					'id': pid
					},                   
					success: function(data)
								{
									//alert("Property Deleted");
									location.reload(true);
								}
				});
			}
        }
</script>
<div id="middle">
	<!-- Insert content for browse page below here. -->
	<table style="width:90%; margin-left: 5%; margin-right: 5%;" cellspacing="0" id="ceotable">
	<tr>
		<td colspan="2"></td>
		<td colspan="3">Add New Property</td>
		<td><a href="add.php"><i class="fa fa-plus" style="color:green; font-size: 300%;"></i></a></td>
	</tr>
		<?php
			/* This pdo statement is used to query the database for all locations data. */
			$stm = $pdo->prepare("SELECT * FROM properties");
			$stm->execute();
			$data = $stm->fetchAll();
			foreach($data as $row){
			echo <<< END
					<tr>
					</td>
						<td rowspan="2" style="width: 200px;"><img id="browseimg" src="images/properties/$row[propertiesID]/1.jpg"></td>
						<td colspan="2"><b>Property ID:</b> $row[propertiesID]</td>
						<td rowspan="2"><a href="details.php?id=$row[propertiesID]"><i class="fa fa-eye" style="color:white; font-size: 300%;"></i></a></td>
						<td rowspan="2"><a href="edit.php?id=$row[propertiesID]"><i class="fa fa-pencil" style="color:#FFCC00; font-size: 300%;"></i></a></td>
						<td rowspan="2"><i class="fa fa-times" style="color:red; font-size: 300%;" onclick="deleteproperty($row[propertiesID])"></i></td>
					</tr>
					<tr>
						<td>$row[StreetAddress]</a></td>
						<td>$row[Suburb]</a></td>
					</tr>
END;
			}
		?>
		</table>
		<hr width="90%">
		<!-- Insert content for browse page above here. -->
	</div>
<?php
	include "footer.php";
?>	