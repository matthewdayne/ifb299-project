<div id="searchDiv" class="searchDiv">
	<form method="post" action="results.php" class="searchbox" style="margin-bottom:0px;">
	<div style="float:centre;">&nbsp;<input type="text" id="suburbText" name="suburb" placeholder="Suburb" style="width:70%; border:2px solid #456879; border-radius:10px; height:30px;"> </div>
	<div style="float:centre;">&nbsp;<input type="submit" name="submit" value="View" class="searchButton"></div>
	<div class = "refineHover" style="font-weight:bold;">
		<input type="checkbox" id="refine"></input>
		<label for="refine">
			<span class='expand' style="color:white;">
				<span class="changeArrow arrow-up">^</span>
				<span class="changeArrow arrow-dn">v</span>
				Refine Search
			</span>
		</label>
	<br>
	<div class="refineSearchDiv">
		<fieldset id="refineSearchDive">
			<div style="float:left;">&nbsp;
				<select name="minAmount" readonly style="border:2px solid #456879; border-radius:10px; font-weight:bold;">
					<option value="">Min p/w</option>
					<option value="">Any</option>
					<option value="50.00">$50pw</option>
					<option value="100.00">$100pw</option>
					<option value="150.00">$150pw</option>
					<option value="200.00">$200pw</option>
					<option value="250.00">$250pw</option>
					<option value="300.00">$300pw</option>
					<option value="350.00">$350pw</option>
					<option value="400.00">$400pw</option>
					<option value="450.00">$450pw</option>
					<option value="500.00">$500pw</option>
					<option value="550.00">$550pw</option>
					<option value="600.00">$600pw</option>
					<option value="650.00">$650pw</option>
					<option value="700.00">$700pw</option>
					<option value="750.00">$750pw</option>
					<option value="800.00">$800pw</option>
					<option value="850.00">$850pw</option>
					<option value="900.00">$900pw</option>
					<option value="950.00">$950pw</option>
					<option value="1000.00">$1000pw</option>
					<option value="1100.00">$1100pw</option>
					<option value="1200.00">$1200pw</option>
					<option value="1300.00">$1300pw</option>
					<option value="1400.00">$1400pw</option>
					<option value="1500.00">$1500pw</option>
					<option value="1600.00">$1600pw</option>
					<option value="1700.00">$1700pw</option>
					<option value="1800.00">$1800pw</option>
					<option value="1900.00">$1900pw</option>
					<option value="2000.00">$2000pw</option>
					<option value="2500.00">$2500pw</option>
					<option value="3000.00">$3000pw</option>
					<option value="3500.00">$3500pw</option>
					<option value="4000.00">$4000pw</option>
					<option value="4500.00">$4500pw</option>
					<option value="5000.00">$5000pw</option>
				</select>
			</div>
			
			<div style="float: left;">&nbsp;
				<select name="maxAmount" readonly style="border:2px solid #456879; border-radius:10px; font-weight:bold;">
					<option value="">Max p/w</option>
					<option value="">Any</option>
					<option value="50.00">$50pw</option>
					<option value="100.00">$100pw</option>
					<option value="150.00">$150pw</option>
					<option value="200.00">$200pw</option>
					<option value="250.00">$250pw</option>
					<option value="300.00">$300pw</option>
					<option value="350.00">$350pw</option>
					<option value="400.00">$400pw</option>
					<option value="450.00">$450pw</option>
					<option value="500.00">$500pw</option>
					<option value="550.00">$550pw</option>
					<option value="600.00">$600pw</option>
					<option value="650.00">$650pw</option>
					<option value="700.00">$700pw</option>
					<option value="750.00">$750pw</option>
					<option value="800.00">$800pw</option>
					<option value="850.00">$850pw</option>
					<option value="900.00">$900pw</option>
					<option value="950.00">$950pw</option>
					<option value="1000.00">$1000pw</option>
					<option value="1100.00">$1100pw</option>
					<option value="1200.00">$1200pw</option>
					<option value="1300.00">$1300pw</option>
					<option value="1400.00">$1400pw</option>
					<option value="1500.00">$1500pw</option>
					<option value="1600.00">$1600pw</option>
					<option value="1700.00">$1700pw</option>
					<option value="1800.00">$1800pw</option>
					<option value="1900.00">$1900pw</option>
					<option value="2000.00">$2000pw</option>
					<option value="2500.00">$2500pw</option>
					<option value="3000.00">$3000pw</option>
					<option value="3500.00">$3500pw</option>
					<option value="4000.00">$4000pw</option>
					<option value="4500.00">$4500pw</option>
					<option value="5000.00">$5000pw</option>
				</select>
			</div>
			
			<div style="float:left;">&nbsp;
				<select name="beds" readonly style="border:2px solid #456879; border-radius:10px; font-weight:bold;">
					<option value="">Beds</option>
					<option value="">Any</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				
				</select>
			</div>
			<div style="float:left;">&nbsp;
				<select name="baths" readonly style="border:2px solid #456879; border-radius:10px; font-weight:bold;">
					<option value="">Baths</option>
					<option value="">Any</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
				
				</select>
			</div>
			<div style="float:left;">&nbsp;
				<select name="parking" readonly style="border:2px solid #456879; border-radius:10px; font-weight:bold;">
					<option value="">Parking</option>
					<option value="">Any</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
				
				</select>
			</div>
			
			<div style="float:left;">
				&nbsp;
				<select name ="type[]" multiple style="border:3px solid #456879; border-radius:5px;">
				<option value="Unit">Unit</option>
					<option value="Apartment">Apartment</option>
					<option value="Townhouse">Townhouse</option>
					<option value="House">House</option>
				</select>
			</div>
			<div style="float:left;">
				&nbsp;
				<select name="furnished" readonly style="border:2px solid #456879; border-radius:10px; font-weight:bold;">
					<option value="">Furnished</option>
					<option value="">Any</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</div>
			<div style="float:left;">&nbsp;<input type="submit" name="submit" value="Search" class="searchButton"></div>

		</fieldset>
	</div>
	</form>
	</div>
</div>	