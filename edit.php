<?php
session_start();
if(!($_SESSION['username']=='DaveCEO' && $_SESSION['password']=='JOHNCENA')) {
	header ("Location: signin.php");
}

include "header.php";

$propertyId=$_GET["id"];
?>
<div id="middle">

<script type="text/javascript" >

        function updateProperty(form) 
        {
            $.ajax({
                url: "inc/update.php",
                type: "POST",
                data: { 
				'id': form.id.value,
				'address': form.address.value,
				'suburb': form.suburb.value,
				'type': form.type.value,
				'bedrooms': form.bedrooms.value,
				'bathrooms': form.bathrooms.value,
				'parking': form.parking.value,
				'rent': form.rent.value,
				'description': form.description.value
				},                   
                success: function(data)
                            {
                                alert("Property Updated");                                    
                            }
            });
        }
		function deleteimage(propImg) 
		{	
			r = confirm("Are you sure you want to delete this image?");
			if (r == true){
				$.ajax({
					url: "inc/deleteimage.php",
					type: "POST",
					data: { 
					'file': propImg
					},                   
					success: function(data)
								{
									location.reload(true);
								}
				});
			}
        }
</script>

<table style="width:90%; margin-left: 5%; margin-right: 5%;" cellspacing="0">
		<?php
			/* This pdo statement is used to query the database for all locations data."></td> */
			$stm = $pdo->prepare("SELECT * FROM properties WHERE propertiesId=$propertyId");
			$stm->execute();
			$data = $stm->fetchAll();
			foreach($data as $row){
			echo <<< END
	
			<TH COLSPAN=5><hr>
			<tr>
			<form action="javascript:updateProperty(this.form);">
			<td><input type="text" name="id" value="$row[propertiesID]" readonly></td>
			<td><input type="text" name="address" value="$row[StreetAddress]"></td>
			<td><input type="text" name="suburb" value="$row[Suburb]"></td>
			<td><input type="text" name="type" value="$row[Type]"></td>
			</tr>
			<tr>
			<td><input type="number" name="bedrooms" value="$row[Bedrooms]" min="1" max="10"> Bedrooms</td>
			<td><input type="number" name="bathrooms" value="$row[Bathrooms]" min="1" max="10"> Bathrooms</td>
			<td><input type="number" name="parking" value="$row[Parking]" min="1" max="10"> Parking</td>
			<td><input type="number" name="rent" value="$row[Rent]" min="0" max="5000"> Rent</td>
			<td><input type="text" name="description" value="$row[Description]"></td>
			
			<td><input type="submit" name="" value="save edit" onclick=updateProperty(this.form);></td>
			</form>
			</tr>
END;
			}
echo <<< END
		</table>
		<form action="inc/upload.php" method="post" enctype="multipart/form-data">
			Select image to upload:
			<input type="file" name="fileToUpload" id="fileToUpload">
			<input type="hidden" name="propID" value="$propertyId">
			<input type="submit" value="Upload Image" name="submit">
		</form>
END;

$propertyId=$_GET["id"];

	$files = glob('images/properties/'. $propertyId .'/*.{jpg,png,gif}', GLOB_BRACE);
	foreach($files as $file) {
	  echo <<< END
	  <div class="propEditImgContainer">
		<div class="propEditImgHover" onclick="deleteimage('$file')"><br><i class="fa fa-times" style="color:red; font-size: 500%;"></i></div>
		<img src="$file" class="propEditImg">
	  </div>
END;
	}
?>
<div style="clear:both;"></div>
	</div>
<?php
	include "footer.php";
?> 	